import Vue from 'vue'
import Vuex from 'vuex'
const axios = require('axios').default;

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    matters: [{ _id: '24dsfsdf52df', name: 'Matematicas'}],
    matter: {
      name: '',
      description: ''
    },
    groups: [],
    group: {
      name: '',
      description: '',
      dateStart: null,
      matter: '',
      students: [],
      active: true
    },
    groupsUser: [],
    personalData: {
      name: '',
      username: '',
      email: '',
      groups: [],
    },
    userData: {
      name: '',
      username: '',
      email: '',
      password: '',
      active: true,
      groups: []
    },
    users: [],
    response: {}
  },
  mutations: {
    resetMatter(state) {
      state.matter = {
        name: '',
        description: ''
      }
    },
    resetGroup(state) {
      state.group = {
        name: '',
        description: '',
        dateStart: null,
        matter: '',
        students: [],
        active: true,
      }
    },
    resetUser(state) {
      state.userData = {
        name: '',
        username: '',
        email: '',
        password: '',
        active: true,
        groups: []
      }
    },
    setResponse(state, response) {
      state.response = response
    },
    setMatters(state, matters) {
      state.matters = matters
    },
    setGroups(state, groups) {
      state.groups = groups
    },
    setUsers(state, users) {
      state.users = users
    }
  },
  actions: {
    createMatter: async function({ commit }, matter) {
      axios.post('http://localhost:1337/matter/create', matter)
          .then(response => commit('setResponse', response.data))
          .catch(err => commit('setResponse', { success: false, data: err }))
    },
    createGroup({ commit }, group) {
      group.dateStart = group.dateStart.split('-').reverse().join('/')
      if (group.students.length > 0)
        group.students = group.students.map(student => student.id)
      axios.post('http://localhost:1337/groups/create', group)
          .then(response => commit('setResponse', response.data))
          .catch(err => commit('setResponse', { success: false, data: err }))
    },
    createUser({ commit }, user) {
      user.groups = user.groups.map(group => group.id)
      axios.post('http://localhost:1337/account/create', user)
          .then(response => commit('setResponse', response.data))
          .catch(err => commit('setResponse', { success: false, data: err }))
    },
    getAllMatters({ commit }) {
      axios.get('http://localhost:1337/matter')
          .then(response => commit('setMatters', response.data))
          .catch(err => commit('setMatters', { success: false, data: err }))
    },
    getAllGroups({ commit }) {
      axios.get('http://localhost:1337/groups')
          .then(response => commit('setGroups', response.data))
          .catch(err => commit('setGroups', { success: false, data: err }))
    },
    getAllUsers({ commit }) {
      axios.get('http://localhost:1337/accounts')
          .then(response => commit('setUsers', response.data))
          .catch(err => commit('setUsers', { success: false, data: err }))
    },
    updateMatter({ commit }, matter) {
      let id = matter.id
      delete matter.id
      console.log(matter)
      axios.put(`http://localhost:1337/matter/${id}`, matter)
          .then(response => commit('setResponse', response.data))
          .catch(err => commit('setResponse', { success: false, data: err }))
    }
  },
  modules: {
  }
})
