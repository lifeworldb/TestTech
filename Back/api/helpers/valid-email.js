module.exports = {


  friendlyName: 'Valid email',


  description: 'Receive a string and valid if correct string email',


  inputs: {
    email: {
      friendlyName: 'Email',
      description: 'Email to validate',
      type: 'string'
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    return exits.success(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(inputs.email))
  }



};

