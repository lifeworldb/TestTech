/**
 * AccountController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  login: async (req, res) => {
    let params = req.allParams()

    if (!params.email || !await sails.helpers.validEmail(params.email))
      return res.status(401).send({ success: false, message: 'Mail is required or is not valid', status: 401 })

    if (!params.password || params.password.length <= 5)
      return res.status(401).send({ success: false, message: 'the password is very short', status: 401 })

    async.waterfall([
      function existsUser(cb) {
        UserService.findByEmail(params.email).then($user => {
          if (!$user)
            return cb(null, { success: false, message: 'User does not exist', status: 404 })
          return cb(null, $user)
        }).catch(cb)
      },
      function validPass($user, cb) {
        UserService.validPassword($user.password, params.password).then($valid => {
          if (!$valid)
            return cb({ success: false, message: 'Password is invalid', status: 401 })
          return cb(null, $user)
        })
      }
    ],
      function done(err, result) {
        if (err && err.status)
          return res.status(err.status).send(err)
        else if (err)
          return res.status(500).send({ success: false, message: 'Internal server error', developMessage: err, status: 500 })

        res.json(result)
      })
  },

  create: async (req, res) => {
    let params = req.allParams()

    if (!params.name || params.name.length <= 3)
      return res.status(401).send({ success: false, message: 'The name is mandatory or it is very short', status: 401 })

    if (!params.username || params.username.length <= 3)
      return res.status(401).send({ success: false, message: 'The username is mandatory or it is very short', status: 401 })

    if (!params.password || params.password.length < 5)
      return res.status(401).send({ success: false, message: 'The password is very short', status: 401 })

    if(!params.email || !await sails.helpers.validEmail(params.email))
      return res.status(401).send({ success: false, message: 'Mail, is required or is not valid', status: 401 })

    async.waterfall([
      function existsByUsername(cb) {
        UserService.existsByUserName(params.username).then($existsByUserName => {
          if ($existsByUserName)
            return cb({ success: false, message: 'Username already exists', status: 401})
          return cb(null, $existsByUserName)
        }).catch(cb)
      },
      function existsByEmail($existsByUserName, cb) {
        UserService.existsByEmail(params.email).then($exists => {
          if ($exists)
            return cb({ success: false, message: 'User already exists', status: 401})

          return cb(null, $exists)
        }).catch(cb)
      },
      function createHash($exists, cb) {
        UserService.password(params.password).then($HASHpassword => {
          return cb(null, $HASHpassword)
        }).catch(cb)
      },
      function insertUser($HASHpassword, cb) {
        UserService.create(params, $HASHpassword).then($user => {
          delete $user.password
          return cb(null, $user)
        }).catch(cb)
      }
    ],
      function done(err, result) {
        if (err && err.status)
          return res.status(err.status).send(err)
        else if (err)
          return res.status(500).send({ success: false, message: 'Internal server error', developMessage: err, status: 500 })
        res.json(result)
      })

  }

};

