/**
 * MatterController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  index: async (req, res) => {
    let matters = await Matter.find()
    await res.json(matters)
  },

  create: async (req, res) => {
    console.log(req.body)
    let params = req.allParams()

    if (!params.name || params.name.length <= 3)
      return res.status(401).send({ success: false, message: 'The name is mandatory or it is very short', status: 401 })

    if (!params.description || params.description.length < 20)
      return res.status(401).send({ success: false, message: 'The description is mandatory or it is very short', status: 401 })

    async.waterfall([
      function createMatter(cb) {
        MatterService.create(params).then($matter => {
          return cb(null, $matter)
        }).catch(cb)
      }
    ],
      function done(err, result) {
        if (err && err.status)
          return res.status(err.status).send(err)
        else if (err)
          return res.status(500).send({ success: false, message: 'Internal server error', developMessage: err, status: 500 })
        res.json(result)
      })
  }

};

