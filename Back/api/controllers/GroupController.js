/**
 * GroupController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  index: async (req, res) => {
    let groups = await Group.find()
    await res.json(groups)
  },

  create: async (req, res) => {
    let params = req.allParams()

    if (!params.name || params.name.length <= 3)
      return res.send(401).send({ success: false, message: 'The name is mandatory or it is very short', status: 401 })

    if (!params.description || params.description.length < 20)
      return res.status(401).send({ success: false, message: 'The description is mandatory or it is very short', status: 401 })

    if (!params.dateStart || !await sails.helpers.validDate(params.dateStart))
      return res.status(401).send({ success: false, message: 'The date is mandatory or date format is not valid', status: 401 })

    async.waterfall([
      function createGroup(cb) {
        GroupService.create(params).then($group => {
          return cb(null, $group)
        }).catch(cb)
      }
    ],
      function done(err, result) {
        if (err && err.status)
          return res.status(err.status).send(err)
        else if (err)
          return res.status(500).send({ success: false, message: 'Internal server error', developMessage: err, status: 500 })
        res.json(result)
      })
  }

};

