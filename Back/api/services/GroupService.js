const moment = require('moment');
const ObjectId = require('mongodb').ObjectID

module.exports = {
  update: (groupId, params) => {
    return new Promise((resolve, reject) => {
      let db = Group.getDatastore().manager
      let collection = db.collection(Group.tableName)

      let query = { $set:{} }

      if (params.name) query.$set.name = params.name
      if(params.description) query.$set.description = params.description
      if (params.dateStart) query.$set.dateStart = params.dateStart
      if(params.active) query.$set.active = params.active
      if(params.matter) query.$set.matter = params.matter

      collection.findAndModify({
        _id: ObjectId(groupId.toString())
      },{
        // sort
      }, query, (err, result) => {
        if (groupId.toString() === '')
          return reject({ success: false, message: 'GroupId is required', status: 401 })
        else if (err)
          return reject({ success: false, message: 'Internal server error', developMessage: err.errmsg, status: 500 })

        if (result && result.value)
          return resolve(result.value)

        return resolve(false)
      })

    })
  },
  create: (params) => {
    return new Promise((resolve, reject) => {
      let db = Group.getDatastore().manager
      let collection = db.collection(Group.tableName)

      let query = {}

      query.name = params.name
      query.description = params.description
      query.dateStart = params.dateStart
      query.active = params.active
      query.matter = params.matter
      query.students = (params.students) ? params.students:[]
      query.createdAt = moment().format()
      query.updatedAt = moment().format()

      collection.insert(query, (err, result) => {
        if (err)
          return reject({
            success:false,
            message: 'Internal server error',
            developMessage: err.errmsg,
            status: 500
          })

        return resolve(result.ops[0])
      })

    })
  }
}
