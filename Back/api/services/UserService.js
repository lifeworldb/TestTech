const ObjectId = require('mongodb').ObjectID;
const bc = require('bcrypt');
const moment = require('moment');
const saltRounds = 10;

module.exports = {
  update: (userId, params) => {
    return new Promise((resolve, reject) => {
      let db = User.getDatastore().manager
      let collection = db.collection(User.tableName)

      let query = { $set: {} }

      if (params.name) query.$set.name = params.name
      if (params.username) query.$set.username = params.username
      if (params.email) query.$set.email = params.email
      if (params.password) query.$set.password = params.password
      // if (params.password) query.$set.password = params.password

      collection.findAndModify({
        _id: ObjectId(userId.toString()),
        active: true
      },{
        // sort
      }, query,{
        new: true,
        fields: {
          password: 0
        }
      }, (err, result) => {
        if (userId.toString() === '')
          return reject({ success: false, message: 'UserId is required', status: 401})
        else if (err)
          return reject({ success: false, message: 'Internal server error', developMessage: err.errmsg, status: 500 })

        if (result && result.value)
          return resolve(result.value)

        return resolve(false)
      })

    })
  },
  validPassword: (hashDb, plainPassword) => {
    return new Promise((resolve, reject) => {
      bc.compare(plainPassword, hashDb, (err, res) => {
        if (err)
          return reject({
            success: false,
            message: 'Internal server error',
            status: 500
          })

        return resolve(!!res)
      })
    })
  },
  password: (password) => {
    return new Promise((resolve, reject) => {
      bc.genSalt(saltRounds, (err, salt) => {
        if (err)
          return reject({
            success: false,
            message: 'Internal server error',
            status: 500
          })

        bc.hash(password, salt, (err, hash) => {
          if (err)
            return reject({
              success: false,
              message: 'Internal server error',
              status: 500
            })

          return resolve(hash)
        })
      })
    })
  },
  findByEmail: (email) => {
    return new Promise((resolve, reject) => {
      let db = User.getDatastore().manager
      let collection  = db.collection(User.tableName)

      collection.findOne({ active: true, email }, (err, user) => {
        if (err)
          return reject({
            success: false,
            message: 'Internal server error',
            developMessage: err.errmsg,
            status: 500
          })

        return resolve(user)
      })
    })
  },
  existsByEmail: (email) => {
    return new Promise((resolve, reject) => {
      let db = User.getDatastore().manager
      let collection = db.collection(User.tableName)

      collection.count({ email }, (err, total) => {
        if (err)
          return reject({ success: false, message: 'Internal server error', developMessage: err.errmsg, status: 500 })

        return resolve(!!total)
      })
    })
  },
  existsByUserName: (username) => {
    return new Promise((resolve, reject) => {
      let db = User.getDatastore().manager
      let collection = db.collection(User.tableName)

      collection.count({ username }, (err, total) => {
        if (err)
          return reject({
            success: false,
            message: 'Internal server error',
            developMessage: err.errmsg,
            status: 500
          })
        return resolve(!!total)
      })
    })
  },
  create: (params, password) => {
    return new Promise((resolve, reject) => {
      let db = User.getDatastore().manager
      let collection = db.collection(User.tableName)

      let query = {
        name: params.name,
        username: params.username,
        email: params.email,
        createdAt: moment().format(),
        updatedAt: moment().format(),
        password: password,
        active: params.active
      }

      collection.insert(query, function done(err, result) {
        if (err)
          return reject({
            sucess: false,
            message: 'Internal server error',
            developMessage: err.errmsg,
            status: 500
          })

        return resolve(result.ops[0])
      })
    })
  }
}
