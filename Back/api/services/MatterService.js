const moment = require('moment');
const ObjectId = require('mongodb').ObjectID;

module.exports = {
  update: (matterId, params) => {
    return new Promise((resolve, reject) => {
      let db = Matter.getDatastore().manager
      let collection = db.collection(Matter.tableName)

      let query = { $set: {} }

      if (params.name) query.$set.name = params.name
      if (params.description) query.$set.description = params.description
      if (params.groups) query.$set.groups = params.groups

      collection.findAndModify({
        _id: ObjectId(matterId.toString())
      },{
        // sort
      }, query,(err, result) => {
        if (matterId.toString() === '')
          return reject({ success: false, message: 'MatterId is required', status: 401 })
        else if (err)
          return reject({ success: false, message: 'Internal server error', developMessage: err.errmsg, status: 500 })

        if (result && result.value)
          return resolve(result.value)
        return resolve(false)
      })
    })
  },
  create: (params) => {
    return new Promise((resolve, reject) => {
      let db = Matter.getDatastore().manager
      let collection = db.collection(Matter.tableName)

      let query = {
        name: params.name,
        description: params.description,
        groups: (params.groups) ? params.groups:[],
        createAt: moment().format(),
        updatedAt: moment().format()
      }

      collection.insert(query, (err, result) => {
        if (err)
          return reject({
            success: false,
            message: 'Internal server error',
            developMessage: err.errmsg,
            status: 500
          })

        return resolve(result.ops[0])
      })
    })
  }
}
